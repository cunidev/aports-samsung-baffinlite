# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-phone-components
pkgver=0_git20190903
pkgrel=1
_commit="6dd77469a1bf1c0349316f91b69e7da726ae60f8"
pkgdesc="Modules providing phone functionality for Plasma"
arch="all"
url="https://community.kde.org/Plasma/Mobile"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="qt5-qtquickcontrols2 plasma-workspace kactivities plasma-pa plasma-nm libqofono breeze-icons plasma-settings telepathy-ofono"
makedepends="extra-cmake-modules kpeople-dev qt5-qtdeclarative-dev kactivities-dev plasma-framework-dev kservice-dev kdeclarative-dev ki18n-dev kio-dev kcoreaddons-dev kconfig-dev kbookmarks-dev kwidgetsaddons-dev kcompletion-dev kitemviews-dev kjobwidgets-dev solid-dev kxmlgui-dev kconfigwidgets-dev kauth-dev kcodecs-dev kpackage-dev kwindowsystem-dev kdbusaddons-dev knotifications-dev kwayland-dev telepathy-qt-dev libphonenumber-dev"
source="
	$pkgname-$_commit.tar.gz::https://invent.kde.org/kde/plasma-phone-components/-/archive/$_commit/plasma-phone-components-$_commit.tar.gz
	0001-Revert-loader.qml-isn-t-necessary-anymore.patch
	startplasmamobile
	plasma-mobile.desktop
	"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" make install

	install -D -m755 "$srcdir"/startplasmamobile \
		"$pkgdir"/usr/bin/startplasmamobile

	install -Dm644 "$srcdir"/plasma-mobile.desktop \
		"$pkgdir"/usr/share/wayland-sessions/plasma-mobile-2.desktop
}
sha512sums="1693df393b0009d3d3c133e7e480a18b37da796e5dc52deaeef00b9beb3d891e9747838ee7df67c9041a93ad9f081fd8df747d10a05d2ab929a1673b4bc26444  plasma-phone-components-6dd77469a1bf1c0349316f91b69e7da726ae60f8.tar.gz
059c68ccb839cf7f907b575b398c6e9f7294ae8bf0d51b0ac9ad555cfbc297a381ffc2cfa2a6679f2472a1eae210f816be1a065533f44bacc625fc76288bcbb0  0001-Revert-loader.qml-isn-t-necessary-anymore.patch
069485a372df96f1b438c026b259bc81a6b417d59f00ef42cfd38e13f0971ce8c63db1d31c7e9603a9a5e0bfedc35ba4a1f2af041b7e24304899f40c10b87432  startplasmamobile
7f4bdbd30cda4c9e23293b7bb1eb6e8536ada056cb3bcc9a6cc3db7bbc2277eac67b519992b7e46afdf5c720df9c696b43a6a9e9f82ed7ebe3937d8c0bf4d55d  plasma-mobile.desktop"
